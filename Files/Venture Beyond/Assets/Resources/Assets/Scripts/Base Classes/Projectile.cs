﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {



	public bool ScaleDamageByVelocity = false;
	public float Damage = 1f;//the damage the projectile does
	public float Velocity = 1f;//how fast the arrow moves
	public float DestroyAfterTime = 15f;

	public enum damageType {Blunt, Slashing, Piercing, Acid, Fire};
	public damageType Type;//this is the type of damage the attack is doing



	public bool IsMoving = true;//can this projectile move
	public bool FriendlyFire = false;


	Vector3 LastPos;//this holds the last potition to check for hits

	void Awake ()
	{
		LastPos = transform.position;
	}

	void Update ()
	{


		if(DestroyAfterTime > 0)//if the ctime is still going the projectile works
		{
			DestroyAfterTime -= Time.deltaTime;

			if (IsMoving)//if the projectile is moving
			{
				transform.position += transform.forward * Velocity * Time.deltaTime;//move it forwards



				RaycastHit hit;
				if(Physics.Linecast(LastPos, transform.position, out hit))//draw a raycast between the 2 points
				{


					if(hit.transform.tag == "Player")//if the projectile hit the player
					{
						float damageToApply;//this holds the damage that will be applyed to the entity
						
						if (ScaleDamageByVelocity)//if this projectile is scaled my the velocity 
						{
							damageToApply = Damage * Velocity;//times damage by velocity
						}else{
							damageToApply = Damage;//else just use the damage
						}
						
						hit.transform.GetComponent<Player>().ApplyDamage(damageToApply, Type.ToString());//apply damage to the player NOTE:::BUG::: i want it to hit the PlayerCollisionBox not the navmeshagent but for some reason it does

						DestroyProjectile(null);//destroy it




					}else if(hit.transform.tag == "CollisionBox")//if it hits another ai and friendly fire is on
					{
						if(FriendlyFire)
						{
							float damageToApply;//this holds the damage that will be applyed to the entity
							
							if (ScaleDamageByVelocity)//if this projectile is scaled my the velocity 
							{
								damageToApply = Damage * Velocity;//times damage by velocity
							}else{
								damageToApply = Damage;
							}
							
							hit.transform.GetComponent<Entity>().ApplyDamage(damageToApply, Type.ToString());//apply damage to the ai

							DestroyProjectile(hit.transform.gameObject);//destroy it
						}




					}else if(hit.transform != null){//if the projectile hits something
						DestroyProjectile(hit.transform.gameObject);//destroy it
					}
				}


				LastPos = transform.position;//set the last pos so collisions can be check between the object
			}



		}else{// if the time is up its destroyed
			Destroy(gameObject);
		}
	}

	void DestroyProjectile (GameObject hit)//in here goes all the code for when the projectile gets destryoed eg destroying it and any effects /sound
	{
		if(hit != null)
		{
			transform.parent = hit.transform;
		}

		Velocity = 0f;

		//Destroy(gameObject);
	}

	void OnDrawGizmosSelected ()//DEBUG the REDLINE shows the area being checked for collisions
	{
		Gizmos.color = Color.red;
		Gizmos.DrawLine(transform.position, LastPos);
	}
}



