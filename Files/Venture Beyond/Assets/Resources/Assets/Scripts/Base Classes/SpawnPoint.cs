﻿using UnityEngine;
using System.Collections;

public class SpawnPoint : Tile {

	public GameObject Entity;//this holds the entity that needs to be spawned

	public void SpawnEntity()//this is called by the LoadEntitys function in Initialize server script
	{
		GameObject spawnedEntity;//this is holding the current entity that is going to be spawned


		if(Entity.GetComponent<Player>())//if this is spawning the player
		{
			Debug.Log("Spawning Player");
			spawnedEntity = Instantiate(Entity, transform.position, Quaternion.identity) as GameObject;//spawn my player and set the player to be controlled by me
			spawnedEntity.GetComponent<Player>().ControlledLocaly = true;

		}else{//if this is not a player spawn then spawn the entity

			spawnedEntity = Instantiate(Entity, transform.position, Quaternion.identity) as GameObject;
			Debug.Log("Spawned Entity:" + Entity);
		}
	}
	
}
