﻿using UnityEngine;
using System.Collections;

public class Tile : MonoBehaviour {

	public string TileName = "Not Yet Set";
	public enum Type {BaseTile, OverTile, ObjectTile, SpawnPointTile};
	public Type TileType;

	public string ColorCode;

	public bool IsOffSet = false;
	public Vector2 OffSetAmount = new Vector2(0,0);

	public int InstantiationAmount = 1;


}
