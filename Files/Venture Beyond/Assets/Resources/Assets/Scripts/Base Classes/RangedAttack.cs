﻿using UnityEngine;
using System.Collections;

public class RangedAttack : MonoBehaviour {
	
	private bool CanAttack = true;//can the entity make this attack
	private Entity entityComponent;
	
	
	
	private Transform Target;//this holds the target that we are trying to attack
	
	private Vector3 targetDirection;//this holds the direction for the raycast to face so its raycasting towards the target
	private Vector3 hitAreaPosition;//this holds the position the physics.spherecast will create the hitsphere to detect collisions
	
	public GameObject Projectile;
	
	public float ExtraProjectileDamage = 1f;// the damage of this attack NOTE is scaled by the extra damage
	
	public bool ScaleDamageByEntitysPhysique = false;//is this damage by this attack scales by the enemys damage bonus whish is caluclated from the strenght
	
	public float Range = 2f;//this is the range at which the entity can try to attack

	
	public float FireDelay = 0.1f;

	public GameObject FirePoint; //this is where the projectile will be created from
	
	public float CoolDown_Attack = 1f;//this is the cool down for the next attack
	protected float currentCoolDown_Attack = 0f;//this holds the current time
	
	public float CoolDown_Move = 0.7f;//this is the cool down for the next attack
	protected float currentCoolDown_Move = 0f;//this holds the current time
	
	
	void Start ()
	{
		entityComponent = GetComponent<Entity>();//this sets the entity component so it can tell the entity if it can move or not
	}
	
	void Update ()
	{
		//ATTACK COOLDOWN
		if(currentCoolDown_Attack > 0)//if the cool down is still going the entitycant attack
		{
			CanAttack = false;
			currentCoolDown_Attack -= Time.deltaTime;
		}else{//if it has finished the entity can attack again
			CanAttack = true;
		}
		
		
		//MOVE COOLDOWN
		if(currentCoolDown_Move > 0)//if the cool down is still going the entity cant move
		{
			entityComponent.AI_Info.CanMove = false;
			currentCoolDown_Move -= Time.deltaTime;
		}else{// if the cool down is dont the entity can move again
			entityComponent.AI_Info.CanMove = true;
		}
		
		
	}
	
	public virtual void Attack_Behaviors (Transform target)//STARTATTACK is the function where you determine different types(BEHAVIOURS) of attacks EG if the enemy jumps at the player then attacks the jumping part will go here AND the damage part will go in Finish Attack
	{//this is called by the the entity. IT Can be overriden by an attackmelee script//THIS IS THE START OF THE ATTACK 
		if(CanAttack)
		{
			Target = target;//this sets the target so it can be used by the script mainly FinishAttack
			//Need to PLAY ANIMATIONS HERE
			currentCoolDown_Attack = CoolDown_Attack;//sets the cooldowns so the player cant attack and move after before the cooldown has finished
			currentCoolDown_Move = CoolDown_Move;
			
			targetDirection = (Target.position - transform.position).normalized;//gets the direction for the raycast to face so its raycasting towards the target
			
			entityComponent.EntityAnimationController.SetTrigger("Range");//this sets the animation to play NOTE make sure the animation that could play before this are not atomic so they can be interupted and instantly play the attack animation
			
			
			Invoke("Attack_Fire", FireDelay);//call for the projectile to be fired after the fire delay
			
			
		}
		
	}
	
	public virtual void Attack_Fire ()//FINISHATTACK this is where we test for hits on the target and apply damage. ANY special attack moves is dont in the Start Attack Eg jumping at the target before attacking
	{

		GameObject currentProjectile;

		currentProjectile = (GameObject)Instantiate(Projectile, FirePoint.transform.position, Quaternion.LookRotation(new Vector3( targetDirection.x, 0, targetDirection.z ) ));

	}
	
	
	void OnDrawGizmosSelected ()//DEBUG the GreenLine shows the direction the attack is attacking and the lenght is showing how far the range is// the green sphere shows the radius of the attack
	{
		if(!CanAttack)
		{
			Gizmos.color = Color.green;
			Gizmos.DrawLine(transform.position, transform.position + (targetDirection.normalized * Range));
		}
	}
}

