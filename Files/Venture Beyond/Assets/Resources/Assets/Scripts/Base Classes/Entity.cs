﻿using UnityEngine;
using System.Collections;

public abstract class Entity : MonoBehaviour {

	public string EntityName;
	public int EntityID;
	public float CurrentHealth;

	[System.Serializable]
	public class attribute_Stats
	{
		public float Strenght = 5f;
		public float Agility = 5f;
		public float Constitution = 5f;
		public float Wisdom = 5f;
	}
	public attribute_Stats Attributes;// this holds all the attributes for the entity. these attributes are use to calculate speed damage etc



	[System.Serializable]
	public class physique_Stats//Physique refers the form, size, and development of a person's body.
	{
		public float Speed;//the Speed of the player when moving
		public float Damage;//the extre damage the player does
		public float MaxHealth;//the health of the player
		public float Reaction;//the reaction of the player



		[System.Serializable]
		public class damage_resistance// the damage resistances for different damage typres
		{
			public float Blunt = 0f;
			public float Slashing = 0f;
			public float Piercing = 0f;
			public float Acid = 0f;
			public float Fire = 0f;
		}
		public damage_resistance DamageResistances;
	}
	public physique_Stats Physique;


	[HideInInspector]
	public Animator EntityAnimationController;//this holds the animator used for animating the character

	


	public bool ControlledByAI = false;// to see if this is an AI
	[System.Serializable]
	public class ai
	{
		[HideInInspector]
		public UnityEngine.AI.NavMeshAgent navComponent;//this holds the navmesh agent
		[HideInInspector]
		public UnityEngine.AI.NavMeshHit hit;//this if a raycast to the targe it stoped by obsticle

		public Transform Target;//the target of the at. could be player or just a random point on map
		public bool TargetInSight;// this is set to true so the AI knows it can see so it can shoot
		public float DistanceToTarget;//holds the distance to the target
		public bool CanMove = true;
		public float StoppingDistance = 1f;



		public enum entityBehaviours {Neutral, Curious, Hostile};
		public entityBehaviours Behaviours;//these are the different behavious a ai entity can have

		public float AwaranceRange = 20f;// this is the bubble of awarness when a player exters it it will start to be attacked

		public MeleeAttack Attack_Melee;
		public RangedAttack Attack_Range;

		//public RangedAttack Attack_Ranged;


	}
	public ai AI_Info;// this holds all the info for the AI



	void Update ()
	{
		if(ControlledByAI)
		{
			AI ();
		}

		CalculatePhysique();//this updates the Physique every frame
	}

	void Awake () {
		EntityAnimationController = transform.GetComponentInChildren<Animator>();//sets the animator
		CalculatePhysique();//calculates physiques eg speed health etc

		CurrentHealth = Physique.MaxHealth;//sets the current health of the entity

		if(ControlledByAI)// if this is being controlled by ai Initilize all required ai components
		{
			AI_Info.navComponent = GetComponent<UnityEngine.AI.NavMeshAgent>();//assignes the navmesh agent for movement
			AI_Info.navComponent.speed = Physique.Speed / 3;//sets the speed it needs to be /3 to scale with the players
			AI_Info.navComponent.acceleration = Physique.Reaction * 2;//sets the acceleration/reaction


			SphereCollider sc =  gameObject.AddComponent<SphereCollider>() as SphereCollider;//adds a awarenessbubble
			sc.isTrigger = true;//sets it to a trigger
			sc.radius = AI_Info.AwaranceRange;//makes the range/radius the awareness range
		}
	}


	private void AI ()//this is called every frame is this entity is ai controlled
	{
		if(AI_Info.Target)
		{
			//MOVING
			if(AI_Info.CanMove)// if the ai can move eg is the ai is shooting a bow it cant move is the ai is charing with a sword it can
			{
				AI_Info.navComponent.SetDestination(AI_Info.Target.position);//tels the ai to start moving to the target else make it stopmoving
			}else{
				AI_Info.navComponent.SetDestination(transform.position);;
			}


			//LINE OF SIGHT
			if (!AI_Info.navComponent.Raycast(AI_Info.Target.position, out AI_Info.hit))//this is defining if the player is in line of sight sett targetinsight to true
			{
				AI_Info.TargetInSight = true;
			}else{
				AI_Info.TargetInSight = false;
			}

			//DISTANCE TO THE TARGET
			AI_Info.DistanceToTarget = AI_Info.hit.distance;//hit is the raycast used for the line of sight it also holds the distance




			//ATTACKING
			if(AI_Info.Behaviours == ai.entityBehaviours.Hostile)// if this is a hostile AI
			{
				//MELEE
				if(AI_Info.Attack_Melee)//if entity has a melee attack
				{
					if(AI_Info.TargetInSight && AI_Info.DistanceToTarget < AI_Info.Attack_Melee.Range )// if the target is in sight and in range of an attack
					{
						AI_Info.Attack_Melee.Attack_Behaviors(AI_Info.Target);//tells the entity to attack with the melee attack
					}
				}

				//RANGED
				if(AI_Info.Attack_Range)//if entity has a Ranged attack
				{
					if(AI_Info.TargetInSight && AI_Info.DistanceToTarget < AI_Info.Attack_Range.Range )// if the target is in sight and in range of an attack
					{
						AI_Info.Attack_Range.Attack_Behaviors(AI_Info.Target);//tells the entity to attack with the Ranged attack
					}
				}
			}

		}

		AnimateEntity(Mathf.Round( AI_Info.navComponent.velocity.z),Mathf.Round( AI_Info.navComponent.velocity.x));//this sends the input of the ai to be animated. if it MAthf>Rounded to make sure the inputs are whole numbers and not .1 etc this makes it less buggy

	}

	public void ApplyDamage(float damage, string damageType)
	{
		float damageToApply = 0f;//this holds the actually damage to take away from the entity after the risistance has been applyed


		if(damageType == "Blunt")//if the damage is blunt damage
		{
			damageToApply = damage - Physique.DamageResistances.Blunt;// - the blunt damage resistance from the damage to apply
		}else if(damageType == "Slashing")//else if the damage type is slashing
		{
			damageToApply = damage - Physique.DamageResistances.Slashing;// - slashing resistance from the damage
		}else if(damageType == "Piercing")//ETC
		{
			damageToApply = damage - Physique.DamageResistances.Piercing;
		}else if(damageType == "Acid")
		{
			damageToApply = damage - Physique.DamageResistances.Acid;
		}else if(damageType == "Fire")
		{
			damageToApply = damage - Physique.DamageResistances.Fire;
		}else{//if the damage isnt any of the damage types for some reason
			damageToApply = damage;//just ,make the damage to apply the actualy damage
		}





		if(damageToApply > 0f)//if the damage is above 0 (this is to make sure it doesnt - -damage to the player and then give him health)
		{
			CurrentHealth -= damageToApply;//- the damage(with the resistances) from the players health
		}

		Debug.Log("Entity:  " + EntityName + "     Has Taken:  " + damageToApply + "  " + damageType + " Damage");
	}

	public void CalculatePhysique ()
	{
		Physique.Speed = Mathf.Floor( Attributes.Agility * 1.5f );
		Physique.Damage = Mathf.Floor(Attributes.Strenght / 2);//rounds down to nearest 1
		Physique.MaxHealth = Mathf.Floor( Attributes.Constitution * 2 );
		Physique.Reaction = Mathf.Floor((Attributes.Agility * 3) + (Attributes.Wisdom) / 2);

	}




	public void AnimateEntity (float VerticalAxisZ, float HorizontalAxisX)//this gets the movement inputs and puts them into mecanim for the character to be animated
	{
		/*Directions: 
		 * idle = 0
		 * Down = 1
		 * up = 2
		 * right = 3
		 * left = 4
		 */

		if(HorizontalAxisX > 0.1f){//if the Entity is moving right set the animation

			EntityAnimationController.SetInteger("Direction", 3);
		}else if(HorizontalAxisX < -0.1f){//if the Entity is moving left set the animation

			EntityAnimationController.SetInteger("Direction", 4);
		}else if(VerticalAxisZ > 0.1f){//if the Entity is moving down set the animation

			EntityAnimationController.SetInteger("Direction", 2);
		}else if(VerticalAxisZ < -0.1f){//if the Entity is moving up set the animation

			EntityAnimationController.SetInteger("Direction", 1);
		}else{// if the entity isnt moving up down left right then its not maving to set the animation to notmoving

			EntityAnimationController.SetInteger("Direction", 0);
		}

	}



	void OnTriggerStay(Collider collider){// if the Player enters the Awarenessbubble 
		if(AI_Info.Behaviours == ai.entityBehaviours.Curious || AI_Info.Behaviours == ai.entityBehaviours.Hostile)
		{
			if (collider.tag == "PlayerCollisionBox")
			{
				AI_Info.Target = collider.transform.parent;//set the target to be the player
			}
		}
	}



	
	void OnTriggerExit(Collider collider){// when the target exits the bubble stop chasing
		if(AI_Info.Target)
		{
			if (collider.transform.parent.gameObject == AI_Info.Target.gameObject)
			{
				AI_Info.Target = null;
			}
		}
	}
}
