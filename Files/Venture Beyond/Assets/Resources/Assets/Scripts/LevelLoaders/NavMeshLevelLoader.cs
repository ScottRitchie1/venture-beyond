﻿using UnityEngine;
using System.Collections;

public class NavMeshLevelLoader : MonoBehaviour {

	//SETTING UP. make sure the background is transparent and there are only 2 pixel of each colour//NOTE:::::::::also make sure there are lots of elemts in NavMeshBuildPoints becasue it cant add items it can only change them


	[System.Serializable]
	public class navpoints
	{
		public Color BuildPointColour;//holds the colour of the matching points
		public Vector2 BuildPoint1;
		public Vector2 BuildPoint2;
	}
	public navpoints[] NavMeshBuildPoints;//this arrya holds all the obsticles points where they will be and also their assigned colour. this array needs to be preset

	public Texture2D levelTexture = null;//holds the texture used to get positions of the obsticales

	public Transform NavMeshObstacle;//this holds the prefab of the obstacle (Scale of 1x1x1. the scale is changed later)
	
	private int levelWidth;
	private int levelHeight;
	
	private Color[] navMeshBuildPointColours;//the pixel colours
	
	public void LoadNavMesh(int LoadLevelId)//this FUNCTION gets the 2(or1) pixels on the texturemap. it matches them up with there 2 Vector2 points and assigned colour
	{
		Debug.Log("loading Nav Mesh...");

		//NavMeshBuildPoints = new navpoints[25];//trying to set array REMOVE THIS LINE

		InitializeLevel Initializer = GetComponent<InitializeLevel>();//this gets the initilizer and gets the correct leveltexture to load
		levelTexture = Initializer.levelInfomation[LoadLevelId].NavMeshPointPointsLevelTexture;

		if(levelTexture == null){
			return;//IF the level doesnt have a navmesh stop here and donbt try to load
		}

		levelWidth = levelTexture.width;
		levelHeight = levelTexture.height;

		navMeshBuildPointColours = new Color[levelWidth * levelHeight];//this sets and gets all the pixels
		navMeshBuildPointColours = levelTexture.GetPixels();
	
		for(int z = 0; z < levelHeight; z++)
		{//these select the current pixel being tested
			for(int x = 0; x < levelWidth; x++)
			{
				if(navMeshBuildPointColours[x+z*levelWidth] != Color.clear)// if it is actually a colour where a point should be and not nothing
				{
					for(int i = 0; i < NavMeshBuildPoints.Length; i++)//check each element in the array for a matching colour
					{
						if(NavMeshBuildPoints[i].BuildPointColour == navMeshBuildPointColours[x+z*levelWidth])//if it found a matching colour
						{
							NavMeshBuildPoints[i].BuildPoint2 = new Vector2(x,z);//set the second point to the point/position of this pixel on the pixel map
							i = NavMeshBuildPoints.Length;//this resets the search to say a matching colour has being found and there is no need to keep searching

						}else if(NavMeshBuildPoints[i].BuildPoint1 == null || NavMeshBuildPoints[i].BuildPointColour == Color.clear)// this is checking if this is an empty spot in the array and this pixel we have it a new one
						{
							NavMeshBuildPoints[i].BuildPointColour = navMeshBuildPointColours[x+z*levelWidth];//assign this element in aur array to this pixel. assign the colour
							NavMeshBuildPoints[i].BuildPoint1 = new Vector2(x,z);//then assign the first point with the position of this pixel
							i = NavMeshBuildPoints.Length;//this is a new pixel and has been assigned there is no need to keep searching
						}

					}
				}
			}
		}
		Debug.Log("Finished Loading Nav Mesh");
		BuildNavMeshFromBuildPoints();//this calls the obstacles to be placed
	}

	private void BuildNavMeshFromBuildPoints ()//this places the obsticles between the 2 points and makes them the correct size
	{
		Debug.Log("Building Nav Mesh...");

		Transform NavMeshObstacleHolder;//holds all the obsticles in the level // HOLDER MUST BE CALLED NavMesh
		NavMeshObstacleHolder = transform.Find("NavMesh").transform;//NavMeshObsticaleholder is alot the navmesh

		Transform currentObstacleBeingBuilt;//this holds the current obstacle being built

		for(int i = 0; i < NavMeshBuildPoints.Length; i++)//for each of the array elements build there obstacle
		{
			if(NavMeshBuildPoints[i].BuildPointColour != Color.clear)//if this has a points assigned to it and isnot empty
			{
				Vector3 NavMeshObstaclePosition;//this holds the position that the Obstical will be created
				Vector3 NavMeshObstacleScale = new Vector3(0, 0, 0);//this holds the scale that the will be added to the Obsticals default scale so it will fit between the points


				if(NavMeshBuildPoints[i].BuildPoint1 != Vector2.zero && NavMeshBuildPoints[i].BuildPoint2 != Vector2.zero)//this is defining if this obstacle is more than 1x1.eg if there are 2 of the same coloured pixels on the leveltexture this obsticle is long and has an average middlepoint and scale. if there is only 1 pixel it doesnt have any average position and scale becasue its just a 1x1x1 cube rather that a 1x1x10 cube
				{
					NavMeshObstaclePosition = new Vector3((NavMeshBuildPoints[i].BuildPoint1.x + NavMeshBuildPoints[i].BuildPoint2.x) / 2, 0, (NavMeshBuildPoints[i].BuildPoint1.y + NavMeshBuildPoints[i].BuildPoint2.y) / 2);//if this isnt a 1x1x1 obstacle find the middle position between the 2 points for this to be created



					if(NavMeshBuildPoints[i].BuildPoint1.x != NavMeshBuildPoints[i].BuildPoint2.x)//if this needs to be scaled on the x axis
					{
						NavMeshObstacleScale.x = Vector2.Distance(NavMeshBuildPoints[i].BuildPoint1, NavMeshBuildPoints[i].BuildPoint2);//find the distance between the 2 x points and the amount needed to add to the scale
					}else{//if this doesnt need to be scaled on the x axis then it must have to be on the y axis (becasue we know this isnt a 1x1x1 cube and it needs to be scaled)
						NavMeshObstacleScale.z = Vector2.Distance(NavMeshBuildPoints[i].BuildPoint1, NavMeshBuildPoints[i].BuildPoint2);//find the distance between the 2 y points and the amount needed to add to the scale
					}
				}else{//if this element did only have one point which means it a 1x1x1 obsticle 
					NavMeshObstaclePosition = new Vector3(NavMeshBuildPoints[i].BuildPoint1.x, 0, NavMeshBuildPoints[i].BuildPoint1.y);//just set the position to be the position of the single pixel/build point
				}





				currentObstacleBeingBuilt = Instantiate(NavMeshObstacle, NavMeshObstaclePosition, Quaternion.identity) as Transform;//create a new obsticle at the correct position 

				currentObstacleBeingBuilt.transform.localScale += NavMeshObstacleScale;//add the scale modifyer to the scale of the obstacle. NOTE THIS ADDS THE THE SCALE IT DOESNT SET IT
				currentObstacleBeingBuilt.transform.parent = NavMeshObstacleHolder;//make the new obsacle a parent of the scene/level so it can be rotated with the tiles/objects
			}else{
				Debug.Log("Finished Building Nav Mesh");
				return;//LINE 79: we have hit a element in our array this doesnt have an assigned colour so it must be the last obstacle. so we finish loading
			}
		}
	}
}
