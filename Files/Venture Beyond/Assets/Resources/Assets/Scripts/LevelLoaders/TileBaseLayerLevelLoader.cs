﻿using UnityEngine;
using System.Collections;
//******** VIEW OBJECTBASELAYERLEVELLOADER for comments ********\\
public class TileBaseLayerLevelLoader : MonoBehaviour {

	private DataBase dataBase;

	public Texture2D levelTexture = null;

	private int levelWidth;
	private int levelHeight;
	
	private Color[] tileColours;
	
	void Awake()
	{
		dataBase = GameObject.FindGameObjectWithTag("DataBase").GetComponent<DataBase>();
	}


	public void LoadBaseLayerTiles(int LoadLevelId)
	{
		Debug.Log("Loading Base Tiles....");

		InitializeLevel Initializer = GetComponent<InitializeLevel>();
		levelTexture = Initializer.levelInfomation[LoadLevelId].BaseTileLevelTexture;

		Transform currentTile;

		Transform BaseTileHolder;//holds all the basetiles in the level // HOLDER MUST BE CALLED BaseTiles
		BaseTileHolder = transform.Find("BaseTiles").transform;

		levelWidth = levelTexture.width;
		levelHeight = levelTexture.height;

		tileColours = new Color[levelWidth * levelHeight];
		tileColours = levelTexture.GetPixels();

		for(int z = 0; z < levelHeight; z++)
		{
			for(int x = 0; x < levelWidth; x++)
			{
				if(tileColours[x+z*levelWidth] != Color.clear)
				{
					for(int t = 0; t < dataBase.BaseTiles.Length; t++)
					{
						if(dataBase.BaseTiles[t].TileType == Tile.Type.BaseTile)
						{
							if(tileColours[x+z*levelWidth].ColorToHex() == dataBase.BaseTiles[t].ColorCode)
							{
								currentTile = Instantiate(dataBase.BaseTiles[t].transform,  new Vector3(x,0,z), Quaternion.identity) as Transform;
								currentTile.parent = BaseTileHolder;
							}
						}
					}

				}
			}
		}

		Debug.Log("Finished Loading Base Tiles");
	}
}
