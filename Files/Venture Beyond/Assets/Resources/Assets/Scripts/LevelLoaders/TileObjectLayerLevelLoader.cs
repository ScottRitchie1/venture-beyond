﻿using UnityEngine;
using System.Collections;

public class TileObjectLayerLevelLoader : MonoBehaviour {

	private DataBase dataBase;//this is the object that holds all data ie entitys items tiles etc

	//holds the height and width of the level texture to keep track of what tile to load next
	private int levelWidth;
	private int levelHeight;

	private Color[] objectColours;// this holds all the colours/spawns for the tiles gotten off the level texture

	public Texture2D levelTexture;// this is a 256x256 texture that is read and gets colours of pixels to determine wot objects to spawn

	void Awake()//::::NOTE IMPORTANT::::: this MUST be awake so it is initilised BEFORE the initilizer level script trys to load the level :::::::::::
	{
		dataBase = GameObject.FindGameObjectWithTag("DataBase").GetComponent<DataBase>();//this finds the database
	}

	public void LoadObjectLayerTiles(int LoadLevelId)
	{
		Debug.Log("Loading Object Tiles....");
		InitializeLevel Initializer = GetComponent<InitializeLevel>();//this holds the initilizer which is for getting the level textures
		levelTexture = Initializer.levelInfomation[LoadLevelId].ObjectTileLevelTexture;//this sets the leveltexture to be loaded 

		Transform currentObject;//this holds the currentobject that is being created so it can be parented anf rotated as one

		Transform ObjectHolder;//holds all the objects in the level // OBJECTS HOLDER MUST be called ObjectTiles
		ObjectHolder = transform.Find("ObjectTiles").transform;//this finds the objectholder
		
		levelWidth = levelTexture.width;//these set the width and the height of the level
		levelHeight = levelTexture.height;

		objectColours = new Color[levelWidth * levelHeight];//this sets the lenght of the array to as many objects that will need to be spawned
		objectColours = levelTexture.GetPixels();//gets the level from the picture and puts it into an array




		for(int z = 0; z < levelHeight; z++)
		{// these 2 lines go through each tile at a time
			for(int x = 0; x < levelWidth; x++)
			{
				if(objectColours[x+z*levelWidth] != Color.clear)// if there is actually a color on this pixel lets assign the correct object to that point
				{



					for(int t = 0; t < dataBase.ObjectTiles.Length; t++)//for each different tile in all the tiles
					{


						if(dataBase.ObjectTiles[t].TileType == Tile.Type.ObjectTile)//if this is a Object tile
						{
							if(objectColours[x+z*levelWidth].ColorToHex() == dataBase.ObjectTiles[t].ColorCode)//if the hex value (colour code) of the pixel is the same as the Colour code/hex of the tile
							{


								for(int i = 0; i < dataBase.ObjectTiles[t].InstantiationAmount; i++)//for each time it has to create a tile
								{
									if(dataBase.ObjectTiles[t].IsOffSet)//check if it has an offset
									{
										//if it has an offset spawn the tile with a random offset ELSE spawn it exactly in the spot
										currentObject = Instantiate(dataBase.ObjectTiles[t].transform,  new Vector3(x + Random.Range(-dataBase.ObjectTiles[t].OffSetAmount.x, dataBase.ObjectTiles[t].OffSetAmount.x) ,0, z + Random.Range(-dataBase.ObjectTiles[t].OffSetAmount.y, dataBase.ObjectTiles[t].OffSetAmount.y)), Quaternion.identity) as Transform;
									}else{
										currentObject = Instantiate(dataBase.ObjectTiles[t].transform,  new Vector3(x,0,z), Quaternion.identity) as Transform;
									}
									currentObject.parent = ObjectHolder;//make the object a parent of the object holder so it can be rotated

								}
							}
						}
					}
				}
			}
		}

		Debug.Log("Finished Loading Object Tiles");
	}
}
