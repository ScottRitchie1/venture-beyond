﻿using UnityEngine;
using System.Collections;
//******** VIEW OBJECTBASELAYERLEVELLOADER for comments ********\\
public class TileOverLayerLevelLoader : MonoBehaviour {
	
	private DataBase dataBase;
	
	public Texture2D levelTexture = null;
	
	private int levelWidth;
	private int levelHeight;
	
	private Color[] tileColours;
	
	void Awake()
	{
		dataBase = GameObject.FindGameObjectWithTag("DataBase").GetComponent<DataBase>();
	}
	
	
	public void LoadOverLayerTiles(int LoadLevelId)
	{
		Debug.Log("Loading Over Tiles....");
		
		InitializeLevel Initializer = GetComponent<InitializeLevel>();
		levelTexture = Initializer.levelInfomation[LoadLevelId].OverTileLevelTexture;
		
		Transform currentTile;
		
		Transform OverTileHolder;//holds all the basetiles in the level // HOLDER MUST BE CALLED BaseTiles
		OverTileHolder = transform.Find("OverTiles").transform;
		
		levelWidth = levelTexture.width;
		levelHeight = levelTexture.height;
		
		tileColours = new Color[levelWidth * levelHeight];
		tileColours = levelTexture.GetPixels();
		
		for(int z = 0; z < levelHeight; z++)
		{
			for(int x = 0; x < levelWidth; x++)
			{
				if(tileColours[x+z*levelWidth] != Color.clear)
				{
					for(int t = 0; t < dataBase.OverTiles.Length; t++)
					{
						if(dataBase.OverTiles[t].TileType == Tile.Type.OverTile)
						{
							if(tileColours[x+z*levelWidth].ColorToHex() == dataBase.OverTiles[t].ColorCode)
							{
								currentTile = Instantiate(dataBase.OverTiles[t].transform,  new Vector3(x + Random.Range(-dataBase.OverTiles[t].OffSetAmount.x, dataBase.OverTiles[t].OffSetAmount.x) ,0, z + Random.Range(-dataBase.OverTiles[t].OffSetAmount.y, dataBase.OverTiles[t].OffSetAmount.y)), Quaternion.identity) as Transform;
								currentTile.parent = OverTileHolder;
							}
						}
					}
					
				}
			}
		}
		
		Debug.Log("Finished Loading Over Tiles");
	}
}
