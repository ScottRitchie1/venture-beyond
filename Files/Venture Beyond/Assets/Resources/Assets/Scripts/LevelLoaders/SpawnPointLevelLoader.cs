﻿using UnityEngine;
using System.Collections;
//******** VIEW OBJECTBASELAYERLEVELLOADER for comments ********\\
public class SpawnPointLevelLoader : MonoBehaviour {

	private DataBase dataBase;//this is the object that holds all data ie entitys items tiles etc

	private int levelWidth;
	private int levelHeight;
	
	private Color[] spawnColours;
	
	public Texture2D levelTexture;

	void Awake()
	{
		dataBase = GameObject.FindGameObjectWithTag("DataBase").GetComponent<DataBase>();//this finds the database
	}

	public void LoadSpawnPoints(int LoadLevelId)
	{
		Debug.Log("Loading Spawn Points ....");
		InitializeLevel Initializer = GetComponent<InitializeLevel>();
		levelTexture = Initializer.levelInfomation[LoadLevelId].SpawnPointLevelTexture;

		Transform currentSpawnPoint;

		Transform spawnPointHolder;//holds all the spawnPoints in the level // SpawnPoint HOLDER MUST be called Spawns
		spawnPointHolder = transform.Find("Spawns").transform;
		
		levelWidth = levelTexture.width;
		levelHeight = levelTexture.height;
		
		spawnColours = new Color[levelWidth * levelHeight];
		spawnColours = levelTexture.GetPixels();//gets the level from the picture and puts it into an array



		for(int z = 0; z < levelHeight; z++)
		{
			for(int x = 0; x < levelWidth; x++)
			{
				if(spawnColours[x+z*levelWidth] != Color.clear)
				{

					for(int t = 0; t < dataBase.SpawnPointTiles.Length; t++)
					{
						if(dataBase.SpawnPointTiles[t].TileType == Tile.Type.SpawnPointTile)
						{
							if(spawnColours[x+z*levelWidth].ColorToHex() == dataBase.SpawnPointTiles[t].ColorCode)
							{
								currentSpawnPoint = Instantiate(dataBase.SpawnPointTiles[t].transform,  new Vector3(x,0,z), Quaternion.identity) as Transform;
								currentSpawnPoint.parent = spawnPointHolder;
							}
						}
					}
				}
			}
		}
		
		Debug.Log("Finished Loading Spawn Points");
	}
}
