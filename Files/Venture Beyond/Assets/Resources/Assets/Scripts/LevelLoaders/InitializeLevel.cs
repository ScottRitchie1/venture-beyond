﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;//allows the use of lists

public class InitializeLevel : MonoBehaviour {

	public List<SpawnPoint> PlayerSpawnPoints;//holds all the possible spawn spots of the player

	public int LoadNextLevel = 0;//the level to load next as per LevelInfomation array




	private TileBaseLayerLevelLoader BaseTile_Loader;//holds all the LevelLoaders to load terrain and entitys
	private TileOverLayerLevelLoader OverTile_Loader;
	private TileObjectLayerLevelLoader ObjectTile_Loader;
	private SpawnPointLevelLoader SpawnPoint_Loader;
	private NavMeshLevelLoader NavMesh_Loader;//holds the navmesh loader which is attached to the navmesh




	[System.Serializable]
	public class lvlinfo
	{
		//these hold all the textures the level loaders use to build the levels
		public Texture2D BaseTileLevelTexture;
		public Texture2D OverTileLevelTexture;
		public Texture2D ObjectTileLevelTexture;
		public Texture2D SpawnPointLevelTexture;
		public Texture2D NavMeshPointPointsLevelTexture;
	}
	public lvlinfo[] levelInfomation;//all the levels








	void Start () {
		//these assign the LevelLoaders
		BaseTile_Loader = GetComponent<TileBaseLayerLevelLoader>();
		OverTile_Loader = GetComponent<TileOverLayerLevelLoader>();
		ObjectTile_Loader = GetComponent<TileObjectLayerLevelLoader>();
		SpawnPoint_Loader = GetComponent<SpawnPointLevelLoader>();
		NavMesh_Loader = transform.GetComponent<NavMeshLevelLoader>();//gets the navmesh compnent on the child with the navmesh




		LoadLevel(LoadNextLevel);//starts to load level Load Next Level
	}



	void LoadLevel(int LoadingLevelId)//this CALLS all the LevelLoaders and tells them to load the level
	{
		BaseTile_Loader.LoadBaseLayerTiles(LoadingLevelId);
		ObjectTile_Loader.LoadObjectLayerTiles(LoadingLevelId);
		OverTile_Loader.LoadOverLayerTiles(LoadingLevelId);
		SpawnPoint_Loader.LoadSpawnPoints(LoadingLevelId);
		NavMesh_Loader.LoadNavMesh(LoadingLevelId);
		
		gameObject.transform.localEulerAngles = new Vector3(0,45,0);//rotates the level 45 degrees 
		
		LoadEntitys();// this calls the entitys to be loaded from the spawnpoints



		Debug.Log ("Successfuly loaded level: " + LoadingLevelId);
	}




	void LoadEntitys ()//Loads entitys
	{
		GameObject SpawnsHolder;//this is the object that holds all the spawn points after they are created by the SpawnPoint_Loader
		SpawnsHolder = transform.Find("Spawns").gameObject;


		foreach(SpawnPoint spawn in SpawnsHolder.GetComponentsInChildren<SpawnPoint>())//for each spawn point in spawn loader
		{
			if(spawn.Entity.GetComponent<Player>() != null)// if this is a players spawn add them to a new list of spawn points for the player
			{
				PlayerSpawnPoints.Add(spawn);
			}else{// if this is not a player spawn point Call the entity to be spawned and remove the spawn point
				spawn.SpawnEntity();
				Destroy(spawn.gameObject);
			}
		}

		PlayerSpawnPoints[Random.Range(0,PlayerSpawnPoints.Count)].SpawnEntity();//PLAYER SPAWNS// randomly select a spawnpoint for the player from the list of possible player spawns and then call the player to be spawned
	}
}
