﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player : Entity {

	public bool ControlledLocaly = false;


	public float Knowledge = 0f;


	[System.Serializable]
	public class userInterface
	{
		public Inventory inventoryHandler;//this holds the inventry ui this player is linked to
		public HUD HUDHandler;//this holds the hud/healthbar the player is linked to
		public InventoryStorage inventoryStorageHandler;//this holds the inventorystoreage script where all the items are held
	}
	public userInterface UI;// this holds all the stuff used for the ui



	void Start ()
	{
		UI.inventoryHandler = GameObject.FindGameObjectWithTag("UI_Inventory").gameObject.GetComponent<Inventory>();//find the inventory gui
		UI.inventoryHandler.LinkedPlayer = gameObject.GetComponent<Player>();//link this play to the ui

		UI.HUDHandler = GameObject.FindGameObjectWithTag("UI_HUD").gameObject.GetComponent<HUD>();
		UI.HUDHandler.LinkedPlayer = gameObject.GetComponent<Player>();

		UI.inventoryStorageHandler = GameObject.FindGameObjectWithTag("UI_InventoryStorage").gameObject.GetComponent<InventoryStorage>();
		UI.inventoryStorageHandler.LinkedPlayer = gameObject.GetComponent<Player>();
	}


	void FixedUpdate()
	{
		if(ControlledLocaly)//if this is my character move him with my inputs
		{
			Move(Input.GetAxis("Vertical"), Input.GetAxis("Horizontal"));
		}

		UpdateUI();//THIS calles the UI to be updated
	}







	private void Move (float RawVertical, float RawHorizontal)//this is called by the script the inputs are y:1 = up y:-1 = down x:1 = right x:-1 = left
	{
		float MovementSpeedX =  RawHorizontal * Physique.Speed;//this calculates the speed the player will be moving at on x axis ie right/left
		float MovementSpeedZ =  RawVertical * Physique.Speed;//this calculates the speed the player will be moving at on y axis ie up/down
		
		float ReactionSpeed =  Physique.Reaction * Time.deltaTime;//this calculates the reaction speed ie the time it takes to get to full speed when moving
		
		
		//this line moves the character players speed is lerped at a rate defined by the reaction speed
		GetComponent<Rigidbody>().velocity = new Vector3(Mathf.Lerp(0,MovementSpeedX, ReactionSpeed), 0, Mathf.Lerp(0, MovementSpeedZ, ReactionSpeed));
		
		
		AnimateEntity(RawVertical, RawHorizontal);//this calls for the character to be animated
	}


	private void UpdateUI ()//this updates the ui
	{
		//ATTRIBUTES UI
		UI.inventoryHandler.UI_Attributes.StrenghtUI_TextField.text = Attributes.Strenght.ToString();
		UI.inventoryHandler.UI_Attributes.AgilityUI_TextField.text = Attributes.Agility.ToString();
		UI.inventoryHandler.UI_Attributes.ConstitutionUI_TextField.text = Attributes.Constitution.ToString();
		UI.inventoryHandler.UI_Attributes.WisdomUI_TextField.text = Attributes.Wisdom.ToString();

		//KNOWLEDGE UI
		UI.inventoryHandler.KnowledgeUI_TextField.text = Knowledge.ToString();

		//PHYSIQUE UI
		UI.inventoryHandler.UI_Physique.DamageUI_TextField.text = Physique.Damage.ToString();
		UI.inventoryHandler.UI_Physique.MaxHealthUI_TextField.text = Physique.MaxHealth.ToString();
		UI.inventoryHandler.UI_Physique.ReactionUI_TextField.text = Physique.Reaction.ToString();
		UI.inventoryHandler.UI_Physique.SpeedUI_TextField.text = Physique.Speed.ToString();

		
		
	}

}
