﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Slot : MonoBehaviour, IPointerClickHandler {//this IPointerClickHandler is to make it a clickable button when you right click

	public Stack<Item> items = new Stack<Item>();//this holds all the items in this slot

	private Text stackText;//this holds the text to display the item stack

	public Sprite EmptySlotSprite;//this holds the sprite of an empty slot

	public bool isEmpty{//this is declaring if this slot is empty
		get { return items.Count == 0;}
	}

	public bool isAvalible
	{
		get{ return CurrentItem.MaxStack > items.Count; }
	}

	public Item CurrentItem
	{
		get {return items.Peek(); }
	}

	void Start ()
	{
		stackText = GetComponentInChildren<Text>();//find the stack text 

		RectTransform slotRect = GetComponent<RectTransform>();//find the size of this slot
		RectTransform textRect = stackText.GetComponent<RectTransform>();//get the size of the text in a child

		int textScale = (int)(slotRect.sizeDelta.x * 0.6);//set the text to be 60% the size of the icon
		stackText.resizeTextMaxSize = textScale;
		stackText.resizeTextMinSize = textScale;

		textRect.sizeDelta = slotRect.sizeDelta;//actually change the size


	}





	private void UseItem()//this is called when you are using an items
	{
		if(!isEmpty)//if the slot that we are trying to use the item isnt empty
		{
			Debug.Log("using items");

			items.Pop().Use();//then remove the items and use it




			if(items.Count > 1)// if ther is more than 1 item in the stack 
			{
				stackText.text = items.Count.ToString();//change the stack text to display how many items are left in the satck
			}else{
				stackText.text = string.Empty;//if ther isnt more than 1 item left then make the string empty
			}
			//stackText.text = items.Count > 1 ? items.Count.ToString() : string.Empty;//




			if (isEmpty)//if the slot after we used the item is empty 
			{
				GetComponent<Image>().sprite = EmptySlotSprite;//then change the sprite to the empty sprite

				transform.parent.GetComponent<InventoryStorage>().EmptySlots += 1;
			}
		}
	}





	public void AddItemToSlot (Item item)//add an item to this slot
	{
		items.Push(item);//add an item to the array
		GetComponent<Image>().sprite = item.IconSprite;//make the sprite the item sprite

		if(items.Count > 1)// if there is more than 1 item in this slot show the stack
		{
			stackText.text = items.Count.ToString();
		}

	}

	public void AddMultipleItemsToSlot (Stack<Item> items)
	{
		this.items = new Stack<Item>(items);
		GetComponent<Image>().sprite = items.Peek().IconSprite;//then change the sprite to the empty sprite


		if(items.Count > 1)// if ther is more than 1 item in the stack 
		{
			stackText.text = this.items.Count.ToString();//change the stack text to display how many items are left in the satck
		}else{
			stackText.text = string.Empty;//if ther isnt more than 1 item left then make the string empty
		}




	}

	public void RemoveItemsFromSlot ()//this removes everything in the slot
	{
		items.Clear();//this removes all the items from the stack
		GetComponent<Image>().sprite = EmptySlotSprite;//this sets the icon to be nothing

		stackText.text = string.Empty;//this sets the stack amount to be nothing
	}
	

	
	public void OnPointerClick (PointerEventData eventData)
	{
		if (eventData.button == PointerEventData.InputButton.Right)//them the mouse right clicks on this button USE THE ITEM IN THIS SLOT
		{

			UseItem();
		}
	}

}
