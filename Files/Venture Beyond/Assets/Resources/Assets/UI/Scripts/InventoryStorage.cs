﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;



//https://www.youtube.com/watch?v=KLaGkc87dDQ
//Up to 2hours11 mins


public class InventoryStorage : MonoBehaviour {
	
	public Player LinkedPlayer;//this holds the player we are linked to

	public GameObject SlotPrefab;//this holds the prefab of the empty slot

	public List<GameObject> SlotList = new List<GameObject>();//this holds all the inventory slots

	[System.Serializable]
	public class inventorystoragelayout_ui
	{
		[HideInInspector]
		public RectTransform inventoryRect;//this is the size of the inventory panel
		[HideInInspector]
		public Vector2 inventorysize;
		
		public int slots;//amount of inventory slots
		public int rows;//the ammount of rows the slots will be displayed in
		
		public float SlotSpacing;//the space between each slot
		
		public float SlotSize;//the size of the slot
	}
	public inventorystoragelayout_ui Layout;//THIS holds the layout of the inventory


	public int EmptySlots; //this holds the ammount of slots that are empty


	//THIS IS FOR MOVING items
	public Slot From = null;
	public Slot To = null;


	public RectTransform DragIcon;


	public GameObject IconMousePrefab;//this is the prefab that is glued to the mouse and will move with it as we move objects around in our inventory
	private GameObject TempIconMousePrefab;//this is the actual prefab that the IconMousePrefab is instantiated to;

	public Canvas canvas;







	void Awake()
	{
		gameObject.tag = "UI_InventoryStorage";
		CreateLayout();//initilize the layout
	}




	void Update ()
	{
		//if (TempIconMousePrefab != null)
		//{
			Vector2 position = (Input.mousePosition - canvas.GetComponent<RectTransform>().localPosition);
			DragIcon.GetComponent<RectTransform>().localPosition = position;

			//RectTransformUtility.ScreenPointToLocalPointInRectangle(canvas.transform as RectTransform, Input.mousePosition, canvas.worldCamera, out position);
		//	//TempIconMousePrefab.transform.position = canvas.transform.TransformPoint(position);
		//}
	}


	public bool AddItem (Item item)//this is called to add an item to the inventory
	{

		if(item.MaxStack == 1)//if the items max stack is only 1 then just put it into a new slot
		{
			AddItemInEmptySlot(item);//place item in new slot
			return true;
		}else{//if the item can be stacked
			AddItemToStackInSlot(item);//try to add it to a stack
			return true;
		}

		return false;
	}









	private void CreateLayout()
	{

		EmptySlots = Layout.slots;

		Layout.inventorysize.x = (Layout.slots / Layout.rows) * (Layout.SlotSize + Layout.SlotSpacing) + Layout.SlotSpacing;//calculates the horzontal size of the inventory window
		Layout.inventorysize.y = Layout.rows * (Layout.SlotSize + Layout.SlotSpacing) + Layout.SlotSpacing;//calculates the vertical size of the inventory window

	
		Layout.inventoryRect = GetComponent<RectTransform>();//these lines set the width and height of the inventory window
		Layout.inventoryRect.sizeDelta = Layout.inventorysize;



		int colums = Layout.slots / Layout.rows;//calculates the colums
		for (int y = 0; y < Layout.rows; y++)
		{
			for (int x = 0; x < colums; x++)
			{
				GameObject newSlot = (GameObject)Instantiate(SlotPrefab);//creates a new slot
				newSlot.name = "slot";//renames it to slot
				newSlot.transform.parent = transform;//makes is it a child of the gameobject


				newSlot.GetComponent<RectTransform>().anchoredPosition = new Vector2(Layout.SlotSpacing * (x + 1) + (Layout.SlotSize * x), -Layout.SlotSpacing * (y + 1) - (Layout.SlotSize * y));//sets its correct position


				SlotList.Add(newSlot);//adds it to the slots
			}
		}
	}





	private bool AddItemToStackInSlot (Item item)//this is trying to add an item to an existing stack
	{
		foreach(GameObject slot in SlotList)//for each slot in the inventory
		{
			Slot tmpSlot = slot.GetComponent<Slot>();//get the slot component 
			
			if(!tmpSlot.isEmpty)//if the item isnot empty then check
			{
				if(tmpSlot.CurrentItem.ItemName == item.ItemName && tmpSlot.isAvalible)// if the items are the same name(which should mean they are the same item) and there is an avalible space in the stack
				{
					tmpSlot.AddItemToSlot(item);//add the item the the stack
					return true;//this is the make sure it doesnt add 2 items 
				}
			}
		}
		if(EmptySlots > 0)//if there are empty spots 
		{
			AddItemInEmptySlot(item);//then add item to empty spots
			return true;
		}

		return false;
	}



	private bool AddItemInEmptySlot (Item item)//this is to plae an item in an empty slot
	{
		if(EmptySlots > 0)//if there are empty slots
		{
			foreach(GameObject slot in SlotList)//for each slot in the array of slots
			{
				Slot tmpSlot = slot.GetComponent<Slot>();

				if(tmpSlot.isEmpty)//if the slot is empty
				{
					tmpSlot.AddItemToSlot(item);//add item to the slot and - 1 from the empty slots
					EmptySlots -= 1;

					return true;
				}
			}
		}

		return false;
	}



	public void MoveItem(GameObject itemMoving)
	{

		if(From == null)
		{
			if(!itemMoving.GetComponent<Slot>().isEmpty)
			{
				From = itemMoving.GetComponent<Slot>();
				From.GetComponent<Image>().color = Color.gray;

				IconMousePrefab = From.gameObject;
				
				Debug.Log (From.name);
				DragIcon.GetComponent<Image>().sprite = From.GetComponent<Image>().sprite;





				/*
				TempIconMousePrefab = (GameObject)Instantiate(IconMousePrefab);
				TempIconMousePrefab.GetComponent<Image>().sprite = itemMoving.GetComponent<Image>().sprite;

				RectTransform tempTransform = TempIconMousePrefab.GetComponent<RectTransform>();
				RectTransform movingItemTransform = itemMoving.GetComponent<RectTransform>();

				tempTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, movingItemTransform.sizeDelta.x);
				tempTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, movingItemTransform.sizeDelta.y);

				TempIconMousePrefab.transform.SetParent(GameObject.Find("_UI").transform, true);

				TempIconMousePrefab.transform.localScale = From.gameObject.transform.localScale;*/


			}
		}else if(To == null){
			To = itemMoving.GetComponent<Slot>();
		}



		if(To != null && From != null){

			Stack<Item> tmpTo = new Stack<Item>(To.items);
			To.AddMultipleItemsToSlot(From.items);

			if(tmpTo.Count == 0)
			{
				From.RemoveItemsFromSlot();
			}else{
				From.AddMultipleItemsToSlot(tmpTo);
			}

			From.GetComponent<Image>().color = Color.white;
			To = null;
			From = null;

			DragIcon.GetComponent<Image>().sprite = null;
			//TempIconMousePrefab = null;
			
		}
	}

}
