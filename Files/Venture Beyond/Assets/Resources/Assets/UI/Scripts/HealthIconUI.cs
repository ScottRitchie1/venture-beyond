﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HealthIconUI : MonoBehaviour {

	public Vector2 UI_Position;
	public Vector2 UI_Size;
	private RectTransform UI_Transform;
	public float LerpSpeed = 5f;

	void Start ()
	{
		UI_Transform = gameObject.GetComponent<RectTransform>();
		UI_Transform.anchoredPosition = Vector2.zero;
	}

	void Update () {
		if(UI_Transform.anchoredPosition != UI_Position)
		{
			UI_Transform.anchoredPosition = Vector2.Lerp(UI_Transform.anchoredPosition, UI_Position, LerpSpeed * Time.deltaTime);
		}

		UI_Transform.sizeDelta = UI_Size;

	}
}
