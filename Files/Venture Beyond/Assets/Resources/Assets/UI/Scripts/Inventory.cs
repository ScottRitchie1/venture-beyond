﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Inventory : MonoBehaviour {

	private bool InventoryShow = false;

	public Player LinkedPlayer;//this holds the player that/entity that is displaying their stats and is in controll of the ui

	public Text KnowledgeUI_TextField;

	[System.Serializable]
	public class attribute_ui
	{
		public Text StrenghtUI_TextField;//these hold the textfield that is a number that will change by the player script
		public Text AgilityUI_TextField;
		public Text ConstitutionUI_TextField;
		public Text WisdomUI_TextField;
	}
	public attribute_ui UI_Attributes;// this holds all the info for the atributes for the ui ie the text that changes for the values


	[System.Serializable]
	public class physique_ui
	{
		public Text SpeedUI_TextField;//these hold the textfield that is a number that will change by the player script
		public Text DamageUI_TextField;
		public Text MaxHealthUI_TextField;
		public Text ReactionUI_TextField;
	}
	public physique_ui UI_Physique;// this holds all the info for the physique for the ui ie the text that changes for the values


	void Update ()
	{
		if(Input.GetKeyDown(KeyCode.I))//if the player presses I
		{
			if(InventoryShow)//if the inventory is showing
			{
				GetComponent<RectTransform>().anchoredPosition = new Vector2(-2000,0);//move it off the screen so its not showing
				InventoryShow = false;

			}else{//if it ISNT showing make it show
				GetComponent<RectTransform>().anchoredPosition = new Vector2(0,0);
				InventoryShow = true;
			}
		}
	}

	public void IncreaseAttribute(string attribute)//THIS is called by the buttons in the inventory ui panel
	{
		if(LinkedPlayer.Knowledge >= 100)//if the player has enough knowledge to upgrade his stats
		{
			if(attribute == "Strenght")//if the attribute that he wants to upgrade is strenght in crease it by 1
			{
				LinkedPlayer.Attributes.Strenght += 1f;


			}else if(attribute == "Agility")
			{
				LinkedPlayer.Attributes.Agility += 1f;


			}else if(attribute == "Constitution")
			{
				LinkedPlayer.Attributes.Constitution += 1f;


			}else if(attribute == "Wisdom")
			{
				LinkedPlayer.Attributes.Wisdom += 1f;


			}else{//THIS IS JUST A DEBUG STATEMENT
				Debug.Log("Couldnot find " + attribute);
			}

			LinkedPlayer.Knowledge -= 100f;//remove 100knowledge from the player
		}
	}

}
