﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class HUD : MonoBehaviour {
	public Player LinkedPlayer;//THIS is the player that is linked and whios health is being displayed



	[System.Serializable]
	public class healthbar_ui
	{
		public HealthIconUI HealthIcon;

		public float HealthIconSpacing = 1f;//the spacing/distance between the hearts
		public Vector2 HealthIconSize = new Vector2(25,25);//the size of the hearts 



		[HideInInspector]
		public List<HealthIconUI> HealthIconsList = new List<HealthIconUI>();//THIS holds all the icons(ACTUALLY THERE HEALTHICONUI SCRIPTS)

		[HideInInspector]
		public Vector2 HealthIconsDisplaySize;//this is the space which the hearts are being displayed in :::Width and Height:::
	}
	public healthbar_ui UI_HealthBar;// this holds all the info for the atributes for the ui ie the text that changes for the values




	void Awake ()
	{
		gameObject.tag = "UI_HUD";
	}







	void Update ()
	{
		if(LinkedPlayer)//if there is a player
		{

			UI_HealthBar.HealthIconsDisplaySize = new Vector2((UI_HealthBar.HealthIconSize.x + UI_HealthBar.HealthIconSpacing) * LinkedPlayer.CurrentHealth, UI_HealthBar.HealthIconSize.y);//this calculates thw width and the height of the area needed to display all the hearths
			//its the width of each heart plus the space inbetween the heart and times that by the amount of hearts FOR THE X......the y is just the height of the icons


			if(LinkedPlayer.CurrentHealth > UI_HealthBar.HealthIconsList.Count)//if the players health is more than amount being displayed add icons
			{
				UI_AddHealthIcon(LinkedPlayer.CurrentHealth - UI_HealthBar.HealthIconsList.Count);//Add as many icons as needed so the amount is equal to the health



			}else if(LinkedPlayer.CurrentHealth < UI_HealthBar.HealthIconsList.Count){//if the amount of icons being displayed is more than the helath delete icons
				UI_RemoveHealthIcon(UI_HealthBar.HealthIconsList.Count - LinkedPlayer.CurrentHealth);//delete as many icons as needed to it matches the players health amount
			}
		}
	}



	//ADD
	void UI_AddHealthIcon (float Ammount)//this ADDS X amound of hearts to the hud display
	{
		Debug.Log("Adding " + Ammount + " Hearts fromUI");


		HealthIconUI current;//this holds the current UIobject we are creating

		for(float i = 0; i < Ammount; i++)//for each heart we want to add
		{
			Debug.Log ("adding heart");



			current = (HealthIconUI)Instantiate(UI_HealthBar.HealthIcon);//Instantiate a new heart as the current object
			current.transform.SetParent(gameObject.transform);//make the current object a parent of the ui this is where all the hearts are stored

			UI_HealthBar.HealthIconsList.Insert(UI_HealthBar.HealthIconsList.Count / 2 ,current);//add this heart to the list of hearts and add it in the center so when it its calculate in will calculate and move to the middle of the lenght of hearts instead of the end
		}

		UI_RecalculateHealthIcon ();//:::RECALCULATE:::recalculate positions for all the hearts


	}


	//REMOVE
	void UI_RemoveHealthIcon (float Ammount)//this REMOVES X amount
	{
		Debug.Log("Removing " + Ammount + " Hearts from UI");


		for(float i = 0; i < Ammount; i++)//for each item we want to remove
		{
			Debug.Log ("Removing heart");


			Destroy(UI_HealthBar.HealthIconsList[UI_HealthBar.HealthIconsList.Count / 2].gameObject);//destroys the middle items OBJECT in the list NOTE this will also be the middle icon being displayed. and you must destroy the object not just the script
			UI_HealthBar.HealthIconsList.RemoveAt(UI_HealthBar.HealthIconsList.Count / 2);//remove the missing object from the same point in the list
		}

		UI_RecalculateHealthIcon ();//:::RECALCULATE:::recalculate positions for all the hearts

	}




	//RECALCULATE
	void UI_RecalculateHealthIcon ()///THIS :::RECALCULATES THE POSISION of all the icons
	{

		for(int i = 0; i < UI_HealthBar.HealthIconsList.Count; i++)//for each item we want to RECALCULATE ITS POSITION note it calculates from the right to the left
		{
			
			UI_HealthBar.HealthIconsList[i].UI_Position = new Vector2((UI_HealthBar.HealthIconSize.x + UI_HealthBar.HealthIconSpacing) * i - UI_HealthBar.HealthIconsDisplaySize.x / 2,0);//the new position of the object is calculated by the width of the icon plus the space bwtwenn the icon times the number of icons we are up to - half the width they are going to be displayed so they are centered
			UI_HealthBar.HealthIconsList[i].UI_Size = UI_HealthBar.HealthIconSize;


		}
	}

}
