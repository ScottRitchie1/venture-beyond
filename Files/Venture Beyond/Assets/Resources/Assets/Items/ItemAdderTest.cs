﻿using UnityEngine;
using System.Collections;

public class ItemAdderTest : MonoBehaviour {

	public Item ItemToAdd;

	void OnTriggerEnter (Collider col)
	{
		Debug.Log(col.name);
		if(col.tag == "PlayerCollisionBox")
		{
			Debug.Log("trying to add " + ItemToAdd.ItemName + " to " + col.transform.parent.name);
			col.transform.parent.GetComponent<Player>().UI.inventoryStorageHandler.AddItem(ItemToAdd);
		}
	}
}
