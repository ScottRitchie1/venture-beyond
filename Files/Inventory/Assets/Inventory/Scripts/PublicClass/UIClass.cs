﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UIClass : MonoBehaviour{

	public static UIClass instance;


	public ItemClass ItemBeingDragged;

	public Transform ThrowPos;

	[System.Serializable]
	public class QuickBar{
		public EquipmentSlotClass QuickEquipPrimary;
		public EquipmentSlotClass QuickEquipSecondary;

		public EquipmentSlotClass QuickConsume1;
		public EquipmentSlotClass QuickConsume2;
		public EquipmentSlotClass QuickConsume3;
		public EquipmentSlotClass QuickConsume4;
	}
	public QuickBar QuickUseBar;

	[System.Serializable]
	public class general_{
		public GameObject Inventory_Panel;
		public GameObject Inventory_Slot;

		public Transform DragIcon;
		public Transform ItemDisplayPanel;

	}
	public general_ General;
	
	void Awake(){
		instance = this;
	}

	void Update (){
		if (ItemBeingDragged) {
			General.DragIcon.position = Input.mousePosition;//makes the dragging icon follow the mouse
			General.DragIcon.parent = General.DragIcon.parent;//this is to keep the draging icon in front

			if(General.ItemDisplayPanel.GetComponent<CanvasGroup> ().alpha == 0f){//if we arnt already displaying the items name
				ShowItemDisplay(ItemBeingDragged);//display the item we are draggings name
			}

		}

		if (General.ItemDisplayPanel.GetComponent<CanvasGroup> ().alpha != 0f) {
			General.ItemDisplayPanel.position = Input.mousePosition;
		}

		//QUICK USE KEYS
		if (Input.GetKeyDown (KeyCode.Keypad1)) {
			UseItemFromSlot(QuickUseBar.QuickEquipPrimary.gameObject);
		}
	}

	public void UseItemFromSlot (GameObject Slot){
		ItemClass ItemInSlot;

		if (Slot.GetComponent<SlotClass> ()) {// if the items we are using is in a normal slot
			SlotClass _SlotClass = Slot.GetComponent<SlotClass> ();//gets the slot
			ItemInSlot = _SlotClass.MyContainer.Container_Slots [_SlotClass.MySlotID].MyItem;//finds the item in the slot

			if (ItemInSlot) {// if there is an item in the slot
				ItemInSlot.OnUseItem ();//use the item 
				_SlotClass.ConfigureSlot (ItemInSlot);//reconfigure slot
			}
		} else if (Slot.GetComponent<EquipmentSlotClass> ()) {// if the item we are using is in an equipment slot IE Quick Use
			EquipmentSlotClass _SlotEClass = Slot.GetComponent<EquipmentSlotClass> ();
			ItemInSlot = _SlotEClass.MyItem;
			
			if (ItemInSlot) {
				ItemInSlot.OnUseItem ();
				_SlotEClass.ConfigureSlot (ItemInSlot);
			}
		}
	}

	public void ShowItemDisplay (ItemClass Item){//this is for when you hover over an item you dfisplay things like its icon damage defence name etc
		Text ItemNameDisplay = General.ItemDisplayPanel.Find ("ItemNameDisplay").GetComponent<Text> ();//gets the text where you will display the name
		General.ItemDisplayPanel.parent = General.ItemDisplayPanel.parent;//makes the item ui on top

		if (Item) {
			General.ItemDisplayPanel.GetComponent<CanvasGroup>().alpha = 1f;//turns it invisable
			ItemNameDisplay.text = Item.ItemName;
			ItemNameDisplay.color = GetItemRarityColor(Item);

		} else {
			General.ItemDisplayPanel.GetComponent<CanvasGroup>().alpha = 0f;//turns it invisable
		}
	}



	/// <summary>
	///  This section is for configuring the inventory planels and hiding and showing them.
	/// </summary>


	public void ShowInventoryPanel (ItemClass _item){
		Configure_Container (_item);
	}

	public void HideInventoryPanel (ItemClass _item){
		Destroy (transform.Find ((_item.ItemName + "_InventoryPanel")).gameObject);
	}


	public void Configure_Container (ItemClass ItemToConfigureIvtPnl){//THIS NEEDS TO BE MOVES TO UI CLASS***************************************************
		GameObject MyCotainerPanel = (GameObject)Instantiate (UIClass.instance.General.Inventory_Panel) as GameObject;
		MyCotainerPanel.transform.SetParent (GameObject.FindWithTag ("UI").transform);
		MyCotainerPanel.transform.localScale = new Vector3 (2, 2, 2);
		MyCotainerPanel.name = ItemToConfigureIvtPnl.ItemName + "_InventoryPanel";
		
		Text MyContainerNameText = MyCotainerPanel.transform.Find ("Name").GetComponent<Text> ();
		Text MyContainerCapacityText = MyCotainerPanel.transform.Find ("Capacity").GetComponent<Text> ();

		MyContainerNameText.text = ItemToConfigureIvtPnl.ItemName + "'s Contents";

		for (int i = 0; i < ItemToConfigureIvtPnl.Container_Slots.Count; i++) {//Creating Slots
			GameObject NewSlot = (GameObject)Instantiate(UIClass.instance.General.Inventory_Slot) as GameObject;//this creaes a new slot called NewSlot
			SlotClass NewSlotClass = NewSlot.GetComponent<SlotClass>();//this holds the new slots SlotCLass Script 
			
			
			NewSlotClass.MyContainer = ItemToConfigureIvtPnl; // this sets the slot so it knows what item this slot belongs to
			NewSlot.transform.SetParent(MyCotainerPanel.transform);
			NewSlot.transform.localScale = new Vector3 (1, 1, 1);
			NewSlotClass.MySlotID = i;//this sets the slots id so it knows where it beilongs in the list of slots
			NewSlot.name = "Slot" + i.ToString();
			
			//Loading Items IntoSlots
			ItemToConfigureIvtPnl.Container_Slots[i].MySlot = NewSlotClass;
			ItemToConfigureIvtPnl.Container_Slots[i].MySlot.ConfigureSlot(ItemToConfigureIvtPnl.Container_Slots[i].MyItem);
		}
	}








	/// <summary>
	/// ********* THIS section is for interacting with slots items and equipmentslots
	/// </summary>
	

	public void InteractWithSlot (SlotClass Slot){
		ItemClass ItemInSlot = Slot.MyContainer.Container_Slots [Slot.MySlotID].MyItem;
		if (ItemBeingDragged) {// if we are dragging an item
			if(Slot.AcceptedItem == ItemBeingDragged.ItemType || Slot.AcceptedItem == ItemClass.Item_Type.All_Items_Accepted){

				if(ItemInSlot == null){// if we have an item and the slot doesnt have an item
					Slot.ConfigureSlot(ItemBeingDragged);
					ItemBeingDragged = null;
					

				}else{//If there is already an item in the slot

					if(ItemBeingDragged.ItemName == ItemInSlot.ItemName && ItemBeingDragged.MaxStack > 1){// if the items are the same and they can be stacked

						if((ItemBeingDragged.currentStack + ItemInSlot.currentStack) <= ItemBeingDragged.MaxStack){// the items can be stacked and still bot eceed the max stack
							ItemInSlot.currentStack += ItemBeingDragged.currentStack;//adds 1 to the stack
							Slot.ConfigureSlot(ItemInSlot);
							Destroy(ItemBeingDragged.gameObject);
							ItemBeingDragged = null;
						}else{
							int difference = ItemInSlot.MaxStack - ItemInSlot.currentStack;//this holds the difference of the items being stacked
							ItemInSlot.currentStack += difference;
							ItemBeingDragged.currentStack -=difference;
							Slot.ConfigureSlot(ItemInSlot);
						}

					}else{// if the items cant be stacked
						ItemClass TempItem = ItemInSlot;//this temperarily holds the item in the slot we are puuting the currently dragged item 
						
						Slot.ConfigureSlot(ItemBeingDragged);
						ItemBeingDragged = TempItem;
					}
				}
			}

		} else if(ItemInSlot){//just pick this item up
			ItemBeingDragged = ItemInSlot;//this sets the item that is being dragged to the item we want to drag
			Slot.ConfigureSlot (null);//this sets the slot the item came from to have nothing in it.
		}

		ConfigureDrag ();//this configures the item being dragged so the icons and stuff are correct
	}

	public void InteractWithEquipmentSlot (EquipmentSlotClass EquipmentSlot){
		if (ItemBeingDragged) {// if we are dragging an item
			if(EquipmentSlot.AcceptedItem == ItemBeingDragged.ItemType || EquipmentSlot.AcceptedItem == ItemClass.Item_Type.All_Items_Accepted){

				if(EquipmentSlot.MyItem == null){// if we have an item and the slot doesnt have an item

					EquipmentSlot.ConfigureSlot(ItemBeingDragged);
					ItemBeingDragged = null;
					
				}else{//If there is already an item in the slot
					if(ItemBeingDragged.ItemName == EquipmentSlot.MyItem.ItemName && ItemBeingDragged.MaxStack > 1){
						if((ItemBeingDragged.currentStack + EquipmentSlot.MyItem.currentStack) <= ItemBeingDragged.MaxStack){
							EquipmentSlot.MyItem.currentStack += ItemBeingDragged.currentStack;//adds 1 to the stack
							EquipmentSlot.ConfigureSlot(EquipmentSlot.MyItem);
							Destroy(ItemBeingDragged.gameObject);
							ItemBeingDragged = null;
						}else{
							int difference = EquipmentSlot.MyItem.MaxStack - EquipmentSlot.MyItem.currentStack;//this holds the difference of the items being stacked
							EquipmentSlot.MyItem.currentStack += difference;
							ItemBeingDragged.currentStack -=difference;
							EquipmentSlot.ConfigureSlot(EquipmentSlot.MyItem);
						}


					}else{
						ItemClass TempItem = EquipmentSlot.MyItem;//this temperarily holds the item in the slot we are puuting the currently dragged item 
						
						EquipmentSlot.ConfigureSlot(ItemBeingDragged);
						ItemBeingDragged = TempItem;
					}
				}
			}
		} else if(EquipmentSlot.MyItem){//just pick this item up
			ItemBeingDragged = EquipmentSlot.MyItem;//this sets the item that is being dragged to the item we want to drag
			EquipmentSlot.ConfigureSlot (null);//this sets the slot the item came from to have nothing in it.
		}
		ConfigureDrag ();//this configures the item being dragged so the icons and stuff are correct
	}

	public void GrabFromStack(SlotClass Slot, int numberToGrab){//THIS is if you right click you can grab X item from the stack, X Being numberToGrab
		ItemClass ItemInSlot = Slot.MyContainer.Container_Slots [Slot.MySlotID].MyItem;

		if (ItemBeingDragged) {// if we hare dragging an item
			if (ItemBeingDragged.ItemName == ItemInSlot.ItemName && ItemBeingDragged.MaxStack > 1) {// if these items can be stacked

				if((ItemInSlot.currentStack - numberToGrab) > 0){// if we are not grabbing the last one

					if ((ItemBeingDragged.currentStack + numberToGrab) <= ItemBeingDragged.MaxStack) {// if the item we are dragging can grab the number of items we want

						ItemBeingDragged.currentStack += numberToGrab;
						ItemInSlot.currentStack -= numberToGrab;
						Slot.ConfigureSlot (ItemInSlot);

					} else {
						int difference = ItemBeingDragged.MaxStack - ItemBeingDragged.currentStack;
						ItemBeingDragged.currentStack += difference;
						ItemInSlot.currentStack -= difference;
						Slot.ConfigureSlot (ItemInSlot);
					}
				}else{// if we are grabbing all the items in the stack
					if((ItemInSlot.currentStack + ItemBeingDragged.currentStack) <= ItemBeingDragged.MaxStack){
						ItemBeingDragged.currentStack += ItemInSlot.currentStack;
						Destroy(ItemInSlot.gameObject);
						Slot.ConfigureSlot(null);
					}
				}

			}
		} else {// if we arent dragging anything and we want to grab from the stack
			GameObject NewItemFromStack = (GameObject)Instantiate(ItemInSlot.gameObject) as GameObject;
			ItemBeingDragged = NewItemFromStack.GetComponent<ItemClass>();//create a new item

			ItemBeingDragged.currentStack = numberToGrab;
			ItemInSlot.currentStack -= numberToGrab;

			if(ItemInSlot.currentStack < 1){// if the stack we got them from is < IE none left there
				Destroy(ItemInSlot.gameObject); // destroy the empty item
				Slot.ConfigureSlot(null);
			}else{
				Slot.ConfigureSlot (ItemInSlot);
			}
		}

		ConfigureDrag ();
	}












	/// <summary>
	/// ************ this section is for picking up and dropping items on the ground NOTE not picking up and dropping in inventory
	/// </summary>
	
	public void DropItem(){//this is called when the player clicks on the dropzone ui area
		if (ItemBeingDragged) {// if the player is going to drop an item
			ItemBeingDragged.transform.position = ThrowPos.transform.position;//THIS NEEDS TO BE CHANGED*************************************
			ItemBeingDragged.transform.localScale = new Vector3(5,5,5);//THIS NEEDS TO BE CHANGED*****************

			ItemBeingDragged.gameObject.SetActive (true);//activate it
			ItemBeingDragged.transform.SetParent(null);

			if(transform.Find ((ItemBeingDragged.ItemName + "_InventoryPanel"))){// if 
				HideInventoryPanel(ItemBeingDragged);
			}

			ItemBeingDragged = null;
			ConfigureDrag ();
			ShowItemDisplay(null);
		}
	}

	public void PickUpItem(ItemClass ItemToPickUp){//this is called from the item being picked up and the item sends it self
		if (ItemBeingDragged == null) {// if we are dragging anything then pick up the item
			ItemToPickUp.transform.SetParent(ThrowPos);
			ItemBeingDragged = ItemToPickUp;
			ConfigureDrag ();
			ItemToPickUp.gameObject.SetActive (false);
		}
	}














	public void ConfigureDrag (){//this is called to configure the drag icon so eaither match the item being dragged or remove the sprite
		if (ItemBeingDragged) {
			General.DragIcon.GetComponent<Image> ().sprite = ItemBeingDragged.ItemIcon;//this just resets the image
			General.DragIcon.GetComponent<CanvasGroup>().alpha = 1f;


			if(transform.Find ((ItemBeingDragged.ItemName + "_InventoryPanel"))){// if 
				HideInventoryPanel(ItemBeingDragged);
			}

			ItemBeingDragged.transform.SetParent(ThrowPos.transform);

		} else {
			General.DragIcon.GetComponent<CanvasGroup>().alpha = 0f;
		}
	}


	public Color GetItemRarityColor (ItemClass item){//this is called to return the items rarity colour
		if (item.ItemRarity == ItemClass.Item_Rarity.Uncommon) {
			return new Color (0.2F, 0.3F, 0.4F, 1F);
		} else if (item.ItemRarity == ItemClass.Item_Rarity.SlightlyRare) {
			return new Color (0.2F, 0.5F, 0.2F, 1F);
		} else if (item.ItemRarity == ItemClass.Item_Rarity.Rare) {
			return new Color (0F, 0.2F, 0.5F, 1F);
		} else if (item.ItemRarity == ItemClass.Item_Rarity.ExtremelyRare) {
			return new Color (0.7F, 0F, 1F, 1F);
		} else if (item.ItemRarity == ItemClass.Item_Rarity.Mythical) {
			return new Color (1F, 1F, 0.3F, 1F);
		} else {//this the items doesnt have a rarity then its just a normal one
			return new Color (0.2F, 0.2F, 0.2F, 1F);
		}
	}

}
