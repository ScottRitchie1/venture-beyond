﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class EquipmentSlotClass : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler {

	public ItemClass MyItem;

	[HideInInspector]
	public Sprite MyDefaultIcon;

	private Image MyImage;
	private Image MyIcon;
	private Image MyOutline;
	private GameObject MyButton;//this holds the button that we click to openup the container the slot is holding
	private Text MyStackText;
	
	public  ItemClass.Item_Type AcceptedItem;//this is a list of items that this slot accepts
	
	
	void Start(){
		MyIcon = transform.Find ("Icon").GetComponent<Image>();
		MyOutline = transform.Find ("OutLine").GetComponent<Image>();
		MyButton = transform.Find ("MyButton").gameObject;
		MyStackText = transform.Find ("StackText").GetComponent<Text> ();
		MyImage = GetComponent<Image> ();


		MyDefaultIcon = MyIcon.sprite;
		MyButton.SetActive (false);
		MyStackText.enabled = false;

		if(MyItem){
			MyItem.transform.SetParent(UIClass.instance.ThrowPos);
			MyItem.gameObject.SetActive(false);
		}

		ConfigureSlot (MyItem);
	}

	public void OnPointerClick (PointerEventData eventData)
	{
		if (eventData.button == PointerEventData.InputButton.Left) {//LEFT CLICK
			UIClass.instance.InteractWithEquipmentSlot (this);
		} else if (eventData.button == PointerEventData.InputButton.Middle) {//MIDDLE CLICK
			if(MyItem){
				if((MyItem.currentStack / 2) > 1){
					//UIClass.instance.GrabFromStack(this, Mathf.CeilToInt(MyItem.currentStack / 2));//grabs half thestack
				}else{
					//UIClass.instance.GrabFromStack(this, 1);//grabs the top item from this stack
				}
			}
		} else if (eventData.button == PointerEventData.InputButton.Right) {//RIGHT CLICK
			
			if(MyItem){
				//UIClass.instance.GrabFromStack(this, 1);//grabs the top item from this stack
			}
		}
	}

	public void OnPointerEnter (PointerEventData eventData)
	{
		MyImage.color = new Color(0.95f,0.95f,0.95f,1f);
		UIClass.instance.ShowItemDisplay (MyItem);
	}
	
	public void OnPointerExit (PointerEventData eventData)
	{
		MyImage.color = Color.white;
		UIClass.instance.ShowItemDisplay (null);
	}

	public void OnClickDisPlayButton ()//this is called when they click the button to display the container
	{
		if (MyItem.Container_Slots.Count > 0) {
			if (UIClass.instance.transform.Find ((MyItem.ItemName + "_InventoryPanel"))) {// if we can find the panel then destroy it if we cant find it then create one
				UIClass.instance.HideInventoryPanel(MyItem);
			}else{
				UIClass.instance.ShowInventoryPanel (MyItem);
			}
		}
	}
	
	
	public void ConfigureSlot (ItemClass ItemToAddToThisSlot){//this configures the slots Graphics and the item we are adding
		
		MyItem = ItemToAddToThisSlot;//this says that this is the item we want in the slot
		
		if (MyItem) {//if we have an item
			Debug.Log ("Configureing Slot: " + gameObject.name + " With: " + MyItem.ItemName);
			
			if(ItemToAddToThisSlot.currentStack > 1){// if this is a stack of items and if we have a custom image for this item stacked
				
				MyStackText.enabled = true;
				MyStackText.text = ItemToAddToThisSlot.currentStack.ToString();
				
				if(ItemToAddToThisSlot.ItemStackIcon){
					MyIcon.sprite = ItemToAddToThisSlot.ItemStackIcon;
				}else{
					MyIcon.sprite = ItemToAddToThisSlot.ItemIcon;
				}
				
			}else{//else set the normal image
				MyStackText.enabled = false;
				MyIcon.sprite = ItemToAddToThisSlot.ItemIcon;
			}
			
			MyOutline.color = UIClass.instance.GetItemRarityColor(ItemToAddToThisSlot);//this gets the items raritycolour and changes the slots outline colour

			if(MyItem.Container_Slots.Count > 0){// if we are a container show the button if not dont show it
				MyButton.SetActive(true);
			}else{
				MyButton.SetActive(false);
			}

		} else {//if we dont have an item then configure the slot to empty
			MyIcon.sprite = MyDefaultIcon;
			MyButton.SetActive(false);
			MyOutline.color = new Color (0.2F, 0.2F, 0.2F, 1F);
			MyStackText.enabled = false;
		}
	}
}
