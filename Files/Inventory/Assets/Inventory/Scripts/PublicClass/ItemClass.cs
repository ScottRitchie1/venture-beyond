﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public abstract class ItemClass : MonoBehaviour{

	public string ItemName = "Item";// this is the name of the item

	//public GameObject Slot;//this holds the slot we are currently in so we can make sure we dont duplicate items *** LOOK IN UICLASS in CONFIGURE DRAG FOR CODE ***


	public Sprite ItemIcon;//this holds the item icon

	public enum Item_Type{HeadGear, ChestGear, LegGear, FeetGear, AccessoryGear, Weapon, Consumable, Other, All_Items_Accepted};// this is the item type NOTE All_Items_Accepted is only used for equipment slots
	public Item_Type ItemType;

	public enum Item_Rarity{Common, Uncommon, SlightlyRare, Rare, ExtremelyRare, Mythical};// this is the rarity IE COLOUR of the item
	public Item_Rarity ItemRarity;

	public int MaxStack = 1;
	//[HideInInspector]
	public int currentStack;//this holds the amount of items currently in this stack
	public Sprite ItemStackIcon;

	[System.Serializable]
	public class _container {
		public ItemClass MyItem;
		public SlotClass MySlot;
	}
	public List<_container> Container_Slots;


	public void OnMouseUp (){//PickUpObject
		UIClass.instance.PickUpItem(this);
	}

	void Start (){
		foreach (_container CurrentSlot in Container_Slots) {//this checks as soon as the game loads if there are any items in this game objects container if there are they are configured
			if(CurrentSlot.MyItem){
				CurrentSlot.MyItem.transform.SetParent(transform);
				CurrentSlot.MyItem.gameObject.SetActive(false);
			}
		}

		if (currentStack < 1) {//this just makes sure that the item when it loads does have atleast 1 item in it
			currentStack = 1;
		}
	}

	public virtual void OnUseItem (){
		if (ItemType == Item_Type.Consumable) {
			Debug.Log ("Used " + ItemName);
			if (currentStack > 1) {
				currentStack -= 1;
			} else {
				Destroy (gameObject);
			}
		} else if(ItemType == Item_Type.AccessoryGear ||ItemType == Item_Type.ChestGear || ItemType == Item_Type.Weapon || ItemType == Item_Type.FeetGear || ItemType == Item_Type.HeadGear || ItemType == Item_Type.LegGear){
			Debug.Log ("Equiped " + ItemName);
			//if()//NOTE************ here we need to check if the item isnt already equipt if it isnt then find an apprepriate eqwuipment slot and equip it to that slot
		}

	}

}
