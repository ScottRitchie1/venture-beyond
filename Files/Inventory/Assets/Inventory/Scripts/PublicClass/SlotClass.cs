﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SlotClass : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler {
	public ItemClass MyContainer;//this holds the item this this slot belongs to
	public int MySlotID;

	private Image MyImage;
	private Image MyIcon;
	private Image MyOutline;
	private Text MyStackText;

	public  ItemClass.Item_Type AcceptedItem;//this is a list of items that this slot accepts
	

	void Awake(){
		MyIcon = transform.Find ("Icon").GetComponent<Image>();
		MyOutline = transform.Find ("OutLine").GetComponent<Image>();
		MyStackText = transform.Find ("StackText").GetComponent<Text> ();
		MyImage = GetComponent<Image> ();
		MyStackText.enabled = false;
	}

	public void OnPointerClick (PointerEventData eventData)
	{
		if (eventData.button == PointerEventData.InputButton.Left) {//LEFT CLICK
			UIClass.instance.InteractWithSlot (this);
		} else if (eventData.button == PointerEventData.InputButton.Middle) {//MIDDLE CLICK
			if(MyContainer.Container_Slots[MySlotID].MyItem){
				if((MyContainer.Container_Slots[MySlotID].MyItem.currentStack / 2) > 1){// this is just to make sure half the stack isnt 0 items which will happen if you try to get 1int / 2int = 0int
					UIClass.instance.GrabFromStack(this, Mathf.CeilToInt(MyContainer.Container_Slots[MySlotID].MyItem.currentStack / 2));//grabs half thestack
				}else{
					UIClass.instance.GrabFromStack(this, 1);//grabs the top item from this stack
				}
			}
		} else if (eventData.button == PointerEventData.InputButton.Right) {//RIGHT CLICK

			if(MyContainer.Container_Slots[MySlotID].MyItem){
				UIClass.instance.GrabFromStack(this, 1);//grabs the top item from this stack
			}
		}
	}

	public void OnPointerEnter (PointerEventData eventData)
	{
		MyImage.color = new Color(0.95f,0.95f,0.95f,1f);
		UIClass.instance.ShowItemDisplay (MyContainer.Container_Slots [MySlotID].MyItem);
	}

	public void OnPointerExit (PointerEventData eventData)
	{
		MyImage.color = Color.white;
		UIClass.instance.ShowItemDisplay (null);
	}






	public void ConfigureSlot (ItemClass ItemToAddToThisSlot){//this configures the slots Graphics and the item we are adding

		MyContainer.Container_Slots[MySlotID].MyItem = ItemToAddToThisSlot;//this says that this is the item we want in the slot

		if (ItemToAddToThisSlot) {//if we have an item
			Debug.Log ("Configureing Slot: " + gameObject.name + " With: " + ItemToAddToThisSlot.ItemName);
			MyIcon.GetComponent<CanvasGroup>().alpha = 1f;

			if(ItemToAddToThisSlot.currentStack > 1){// if this is a stack of items and if we have a custom image for this item stacked

				MyStackText.enabled = true;
				MyStackText.text = ItemToAddToThisSlot.currentStack.ToString();

				if(ItemToAddToThisSlot.ItemStackIcon){
					MyIcon.sprite = ItemToAddToThisSlot.ItemStackIcon;
				}else{
					MyIcon.sprite = ItemToAddToThisSlot.ItemIcon;
				}

			}else{//else set the normal image
				MyStackText.enabled = false;
				MyIcon.sprite = ItemToAddToThisSlot.ItemIcon;
			}


			MyOutline.color = UIClass.instance.GetItemRarityColor(ItemToAddToThisSlot);//this gets the items raritycolour and changes the slots outline colour


			ItemToAddToThisSlot.transform.SetParent(MyContainer.transform);//THses lines are so when the slot configures the item it also disables the item and sets the parent to its container
			ItemToAddToThisSlot.gameObject.SetActive(false);


		} else {//if we dont have an item then configure the slot to empty
			MyIcon.GetComponent<CanvasGroup>().alpha = 0f;
			MyOutline.color = new Color (0.2F, 0.2F, 0.2F, 1F);
			MyStackText.text = "0";
			MyStackText.enabled = false;
		}



	}

}
