﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class DragableUI : MonoBehaviour, IDragHandler, IBeginDragHandler {

	RectTransform m_transform = null;

	void Start () {
		m_transform = GetComponent<RectTransform>();
	}
	
	
	public void OnBeginDrag (PointerEventData eventData)
	{
		transform.parent = transform.parent;
	}
	
	
	public void OnDrag(PointerEventData eventData)
	{
		m_transform.position += new Vector3(eventData.delta.x, eventData.delta.y);
	}
}
